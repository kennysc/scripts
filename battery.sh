#!/bin/bash

# Without remaining time
battery=$(acpi --battery | grep 0: | sed -e 's/,/ /g' -e 's/Discharging/ /' -e 's/Charging//' -e 's/Not\ charging//' | awk '{print $3" "$4}')

# With remaining time
#battery=$(acpi --battery | awk '{print $3 $4 $5}' | sed -e 's/,/ /g' -e 's/Discharging/ /' -e 's/Charging//' -e 's/Notcharging//')

echo $battery
