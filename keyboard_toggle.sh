#!/bin/bash

export current_layout=$(sudo setxkbmap -query | grep layout | awk '{print $2}')

case $current_layout in
	ca) sudo setxkbmap us;;
	us) sudo setxkbmap ca;;
*);;
esac
