#!/bin/bash

act_br=$(cat /sys/class/backlight/intel_backlight/brightness)
max_br=$(cat /sys/class/backlight/intel_backlight/max_brightness)
perc_br=$(( ($act_br*100)/$max_br ))

echo -e " $perc_br%"
