#!/bin/bash

volume=$(amixer get Master | grep "Right: Playback" | sed -r 's/.*\[(.*)%\].*/\1/')
muted=$(amixer get Master | grep "Right: Playback" | sed -r 's/.*\[(.*)\].*/\1/')

if [[ $volume -le 20 ]] && [[ $muted == on ]]; then
	echo " $volume%"
elif [[ $volume -ge 21 ]] && [[ $volume -le 50 ]] && [[ $muted == on ]]; then
	echo " $volume%"
elif [[ $volume -ge 50 ]] && [[ $muted == on ]]; then
	echo " $volume%"
elif [[ $muted == off ]]; then
	echo " muted"
fi
